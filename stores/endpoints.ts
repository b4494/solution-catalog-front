import { useIntegrationStore } from "~~/stores/integration";

export interface Endpoint {
	url: string;
}

export function endpoints() {
	const $intergationStore = useIntegrationStore();
	return {
		// Catalog
		catalog: {
			list: {
				url: `partners/${$intergationStore.INTEGRATION}/catalog`,
			},
			tags: {
				url: `partners/${$intergationStore.INTEGRATION}/catalog/tags`,
			},
		},

		// Solution
		solution: (id: number) => ({
			url: `partners/${$intergationStore.INTEGRATION}/catalog/${id}`,
		}),

		// Session
		session: {
			url: `session`,
		},
	};
}
