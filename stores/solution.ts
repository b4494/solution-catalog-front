import { defineStore } from "pinia";
import { Solution } from "~~/mocks/solution";
import { getSolutionByID } from "~~/services/getDataService";

export const useSolutionStore = defineStore({
	id: "solution",
	state: () => ({
		bot: {} as Solution.Response,
	}),

	getters: {},

	actions: {
		async fetchSolution(botId: number, loader: Loader) {
			this.bot = await getSolutionByID(botId);
			loader.value = false;
		},
	},
});
