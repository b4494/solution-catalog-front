import type { Tags } from "~~/mocks/tags";
import type { CheckboxOption } from "~~/components/input/InputCheckBoxGroup.vue";

import { defineStore } from "pinia";
import { getBotList, getTags } from "~~/services/getDataService";
import { Catalog } from "~~/mocks/catalog";

const asideKeys = ["global", "messenger"];

interface CategoriesGroup {
	group: string;
	categories?: Tags.Tag[];
}
interface FilterOption {
	label: string;
	options: CheckboxOption[];
}

export interface TagGroups {
	[key: string]: Tags.Tag[];
}

export const useCatStore = defineStore({
	id: "catalog",
	state: () => ({
		tags: [] as Tags.Tag[],

		bots: [] as Catalog.Item[],
		filters: {},

		filtersOptions: {} as { [key: string]: FilterOption },
		searchQuery: "",

		pagination: {
			currentPage: 1,
			totalPages: 1,
			totalItems: 0,
		} as Pagination,
	}),

	getters: {
		tagGroups(): TagGroups {
			const tags = {} as TagGroups;

			this.tags.forEach((tag) => {
				if (!tags[tag.ct]) tags[tag.ct] = [];
				tags[tag.ct].push(tag);
			});

			return tags;
		},

		hasTagFilters(): boolean {
			return Object.keys(this.tagGroups).reduce((acc, group) => {
				return acc || !asideKeys.includes(group);
			}, false);
		},

		activeFilters(): Tags.Tag[] {
			const arr = this.tags.filter((tag) => tag._checked);

			if (this.searchQuery) {
				arr.push({ lb: this.searchQuery, ct: "", _search: true });
			}

			return arr;
		},

		getBotByID: (s) => (id: number | undefined) => {
			if (typeof id === "undefined") return id;

			return s.bots.find(($b) => $b.id === id);
		},

		getBotIdBySlug: (s) => (slug: string) => {
			console.log(s.bots);
			console.log(slug);
			return s.bots.find(($b) => $b.slug === slug)?.id;
		},
	},

	actions: {
		async initFetch() {
			return Promise.all([this.fetchTags(), this.fetchCatalog()]);
		},

		async fetchTags() {
			getTags().then((response: Tags.Tag[]) => {
				this.tags = response
					.map((tag) => ({ ...tag, _checked: false }))
					// @ts-ignore
					.sort((a, b) => b.solutions - a.solutions);
			});
		},

		async fetchCatalog(page?: number) {
			if (page) this.pagination.currentPage = page;

			await getBotList({
				search: this.searchQuery,
				tags: this.tags.filter((tag) => tag._checked).map((tag) => tag.id),
				page: this.pagination.currentPage,
			}).then((r) => {
				this.bots = r.items;
				this.pagination.totalPages = r.totalPages;
				if (!this.pagination.totalItems)
					this.pagination.totalItems = r.totalItems;
			});
		},

		clearFilters(filter?: Tags.Tag) {
			//. Delete single filter
			if (filter) {
				if (filter._search) {
					this.searchQuery = "";
				} else {
					this.tags.find((tag) => tag.id === filter.id)!._checked = false;
				}

				return;
			}

			//. Clear all
			this.tags.forEach((tag) => (tag._checked = false));

			this.searchQuery = "";
		},
	},
});
