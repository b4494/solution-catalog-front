import { defineStore } from "pinia";
import type { Session } from "~~/mocks/session";
import { getSession } from "~~/services/getDataService";

export const useIntegrationStore = defineStore({
	id: "integration",
	state: () => ({
		INTEGRATION: "",
		BASE_DOMAIN: "",
		BASE_PATH: "",
		LANG: "",

		hash: {
			user: "",
			app: "",
		},
	}),

	getters: {},

	actions: {
		async init() {
			await this.fetchHeaders();
			await this.fetchSession();
		},

		async fetchHeaders() {
			const $config = useRuntimeConfig();
			return $fetch("/api/headers").then((response) => {
				// prettier-ignore
				this.INTEGRATION = (response?.integration as string) ?? $config.public.defaultIntegration;

				// prettier-ignore
				this.BASE_DOMAIN = (response?.baseDomain as string) ?? $config.public.defaultBaseDomain;

				if (!this.INTEGRATION) {
					throw new Error("Have no x-integration");
				}

				if (!this.BASE_DOMAIN) {
					throw new Error("Have no x-base-domain");
				}
			});
		},

		async fetchSession() {
			return getSession().then((r: Session.Response) => {
				this.hash.user = r?.user?.hash;
				this.hash.app = r?.app?.hash;
			});
		},
	},
});
