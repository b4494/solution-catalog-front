interface Pagination {
	currentPage: number;
	totalPages: number;
	totalItems: number;
}
