export default defineNuxtRouteMiddleware(to => {
	window.self !== window.top && window.parent.postMessage({ routeChanged: to.path }, '*');
});