export default defineNuxtRouteMiddleware(() => {
	const modals = document.querySelectorAll(".modal-backdrop");

	modals.forEach((m) => m.remove());
});
