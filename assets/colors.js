module.exports = {
	white:       "#FFFFFF",
	inherit:     "inherit",
	transparent: "transparent",

	text: {
		"button-secondary": "",
		"button-primary": 	"#FFFFFF",
		label: 							"#344054"
  },
  
	bg: {
		card: 						'#FBFBFB',
		'label-info': 		'#F6F6F6',
		'label-infodark': '#EEEEEE',
		checkbox: 				'#F3F3F3',
		filterchip: 			'#EDF1F4'
	},

	divider: "#EEEEEE",

	primary: {
		DEFAULT: "#594ED7",
		100:     "#F3F4FF",
    200:     "#EDECFF",
    400:     '#6B5EFB',
		500:     "#594ED7",
		600:     "#473EA8",
	},
};
