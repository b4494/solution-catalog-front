export declare namespace Catalog {
	interface Response {
		currentPage: number;
		totalPages: number;
		totalItems: number;
		items: Item[];
	}

	interface Item {
		id: number;
		title: string;
		slug: string;
		icon?: any;
		demo_image?: string;
		video_id?: string;
		story_url?: any;
		short_description: string;
		short_promo_text: string;
		product: Product;
		tags: Tag[];
		demo?: Demo;
	}

	interface Demo {
		telegram: boolean;
		whatsapp?: boolean;
	}

	interface Tag {
		id: number;
		lb: string;
		ct: string;
	}

	interface Product {
		id: number;
		code: string;
		price: number;
		originalPrice: OriginalPrice;
		currentPrice: OriginalPrice;
		discountPrice: OriginalPrice;
		currency: string;
		priceFormatted: string;
		clientsLimit: number;
	}

	interface OriginalPrice {
		amount: number;
		currency: string;
		formatted: string;
	}
}

export default {
	currentPage: 1,
	totalPages: 1,
	totalItems: 9,
	items: [
		{
			id: 15,
			title: "NPS-бот для сбора обратной связи",
			slug: "nps-bot-dlia-sbora-obratnoi-sviazi",
			icon: null,
			demo_image:
				"https://console.bot-marketing.com/storage/1PXqjvoRpXIFKZNMQpgeMf8OdNoayXKjfeyN3lmb.png",
			video_id: "https://youtu.be/hvpr_Savpx8",
			story_url: null,
			short_description:
				"Чат-бот, который отладит процесс получения обратной связи от ваших клиентов.",
			short_promo_text:
				"<p>Ваши клиенты знают, как вам стать лучше. Их отзывы - отличный инструмент и кладезь идей по оптимизации бизнеса. Оставайтесь на связи с вашими пользователями, а мы поможем автоматизировать этот процесс.</p>",
			product: {
				id: 27,
				code: "Чат-бот-ОС-1",
				price: 2499,
				originalPrice: {
					amount: 249900,
					currency: "RUB",
					formatted: "2 499 ₽",
				},
				currentPrice: {
					amount: 249900,
					currency: "RUB",
					formatted: "2 499 ₽",
				},
				discountPrice: {
					amount: 0,
					currency: "RUB",
					formatted: "0 ₽",
				},
				currency: "RUB",
				priceFormatted: "2 499,00 ₽",
				clientsLimit: 0,
			},
			tags: [
				{
					id: 10,
					lb: "Прием обратной связи",
					ct: "business",
				},
			],
			demo: null,
		},
		{
			id: 16,
			title: "Telegram WebApp-магазин",
			slug: "telegram-webapp-magazin",
			icon: null,
			demo_image: null,
			video_id: "https://youtu.be/hvpr_Savpx8",
			story_url: null,
			short_description: "Полноценный интернет-магазин в Telegram.",
			short_promo_text:
				"<p>Ежедневно Telegram используют 41 миллион человек. Как думаете, сколько среди них ваших потенциальных клиентов?&nbsp;</p>\r\n<p>С недавнего времени в Telegram появилась возможность открывать web-app/сайты прямо из интерфейса чат-бота. А значит, теперь в Telegram можно сделать полноценный интернет-магазин.</p>",
			product: {
				id: 20,
				code: "WebApp-магазин-1",
				price: 2500,
				originalPrice: {
					amount: 250000,
					currency: "RUB",
					formatted: "2 500 ₽",
				},
				currentPrice: {
					amount: 250000,
					currency: "RUB",
					formatted: "2 500 ₽",
				},
				discountPrice: {
					amount: 0,
					currency: "RUB",
					formatted: "0 ₽",
				},
				currency: "RUB",
				priceFormatted: "2 500,00 ₽",
				clientsLimit: 0,
			},
			tags: [
				{
					id: 2,
					lb: "Магазин",
					ct: "business",
				},
			],
			demo: null,
		},
		{
			id: 17,
			title: "Чат-бот с аналитикой",
			slug: "cat-bot-s-analitikoi",
			icon: null,
			demo_image:
				"https://console.bot-marketing.com/storage/zy0LlZFVcnX0YWYmxh2pdPUJexRavtyIyXqJm1TG.gif",
			video_id: "https://youtu.be/hvpr_Savpx8",
			story_url: null,
			short_description:
				"Ваш личный аналитик отправит отчет прямо в мессенджер.",
			short_promo_text:
				"<p>Чат-бот проанализирует базу данных и пришлет статистику прямо в чат. Получайте свежую информацию по заказам ежедневно, еженедельно или ежемесячно.&nbsp;</p>",
			product: {
				id: 21,
				code: "Чат-бот-аналитика-1",
				price: 1000,
				originalPrice: {
					amount: 100000,
					currency: "RUB",
					formatted: "1 000 ₽",
				},
				currentPrice: {
					amount: 100000,
					currency: "RUB",
					formatted: "1 000 ₽",
				},
				discountPrice: {
					amount: 0,
					currency: "RUB",
					formatted: "0 ₽",
				},
				currency: "RUB",
				priceFormatted: "1 000,00 ₽",
				clientsLimit: 0,
			},
			tags: [],
			demo: {
				telegram: true,
			},
		},
		{
			id: 18,
			title: "Чат-бот для распределения заявок",
			slug: "cat-bot-dlia-raspredeleniia-zaiavok",
			icon: null,
			demo_image: null,
			video_id: "https://youtu.be/hvpr_Savpx8",
			story_url: null,
			short_description:
				"Решение для бизнесов, работающих с дилерами в разных регионах. Чат-бот распределит заявки в зависимости от географии лида.",
			short_promo_text:
				"<p>Чат-бот будет распределять заказы между дилерами. Как только заказ появляется в Retail CRM, чат-бот выберет соответствующего дилера и отправит ему уведомление. Если дилер нажимает кнопку принять, чат-бот отправит ему подробную информацию по заказу.</p>",
			product: {
				id: 22,
				code: "Чат-бот-РЗ-1",
				price: 10000,
				originalPrice: {
					amount: 1000000,
					currency: "RUB",
					formatted: "10 000 ₽",
				},
				currentPrice: {
					amount: 1000000,
					currency: "RUB",
					formatted: "10 000 ₽",
				},
				discountPrice: {
					amount: 0,
					currency: "RUB",
					formatted: "0 ₽",
				},
				currency: "RUB",
				priceFormatted: "10 000,00 ₽",
				clientsLimit: 0,
			},
			tags: [
				{
					id: 10,
					lb: "Прием обратной связи",
					ct: "business",
				},
				{
					id: 11,
					lb: "Распределение заявок",
					ct: "extensions",
				},
			],
			demo: null,
		},
		{
			id: 19,
			title: "Чат-бот перевода обращения между операторами",
			slug: "cat-bot-perevoda-obrashheniia-mezdu-operatorami",
			icon: null,
			demo_image: null,
			video_id: "https://youtu.be/hvpr_Savpx8",
			story_url: null,
			short_description:
				"Чат-бот уточнит информацию у клиента и назначит заявку на наиболее подходящего оператора",
			short_promo_text:
				"<p>Чат-бот уточняет местоположение клиента и распределяет обращение между операторами из того же региона, находящимися он-лайн. В случае отсутствия ответа от оператора переводит диалог на другого свободного сотрудника.</p>",
			product: {
				id: 23,
				code: "Чат-бот-ПО-1",
				price: 2500,
				originalPrice: {
					amount: 250000,
					currency: "RUB",
					formatted: "2 500 ₽",
				},
				currentPrice: {
					amount: 250000,
					currency: "RUB",
					formatted: "2 500 ₽",
				},
				discountPrice: {
					amount: 0,
					currency: "RUB",
					formatted: "0 ₽",
				},
				currency: "RUB",
				priceFormatted: "2 500,00 ₽",
				clientsLimit: 0,
			},
			tags: [
				{
					id: 11,
					lb: "Распределение заявок",
					ct: "extensions",
				},
			],
			demo: {
				telegram: true,
			},
		},
		{
			id: 20,
			title: "Чат-бот с меню самообслуживания и quiz-опросом",
			slug: "cat-bot-s-meniu-samoobsluzivaniia-i-quiz-oprosom",
			icon: null,
			demo_image: null,
			video_id: "https://youtu.be/hvpr_Savpx8",
			story_url: null,
			short_description:
				"Чат-бот проконсультирует клиента по частым вопросам и проведет quiz-опрос.",
			short_promo_text:
				"<p>Экономит время сотрудников и генерирует лиды. С помощью меню самообслуживания чат-бот проконсультирует клиента, а также проведет quiz-опрос и сформирует заявку.</p>",
			product: {
				id: 19,
				code: "ITR-1",
				price: 2500,
				originalPrice: {
					amount: 250000,
					currency: "RUB",
					formatted: "2 500 ₽",
				},
				currentPrice: {
					amount: 250000,
					currency: "RUB",
					formatted: "2 500 ₽",
				},
				discountPrice: {
					amount: 0,
					currency: "RUB",
					formatted: "0 ₽",
				},
				currency: "RUB",
				priceFormatted: "2 500,00 ₽",
				clientsLimit: 0,
			},
			tags: [
				{
					id: 13,
					lb: "Консультант",
					ct: "extensions",
				},
				{
					id: 15,
					lb: "Рекомендуем 👍",
					ct: "global",
				},
			],
			demo: {
				telegram: true,
				whatsapp: true,
			},
		},
		{
			id: 21,
			title: "Чат-бот, отслеживающий, где посылка",
			slug: "cat-bot-otslezivaiushhii-gde-posylka",
			icon: null,
			demo_image: null,
			video_id: null,
			story_url: null,
			short_description: "Чат-бот для отслеживания посылок",
			short_promo_text:
				"<p>NLP-бот&nbsp;поймет обращение в свободной форме, проверит заказ в базе, определит трек-номер, запросит информацию по заказу в транспортной компании и передаст ее клиенту.&nbsp;</p>",
			product: {
				id: 25,
				code: "Чат-бот-П-1",
				price: 2500,
				originalPrice: {
					amount: 250000,
					currency: "RUB",
					formatted: "2 500 ₽",
				},
				currentPrice: {
					amount: 250000,
					currency: "RUB",
					formatted: "2 500 ₽",
				},
				discountPrice: {
					amount: 0,
					currency: "RUB",
					formatted: "0 ₽",
				},
				currency: "RUB",
				priceFormatted: "2 500,00 ₽",
				clientsLimit: 0,
			},
			tags: [
				{
					id: 14,
					lb: "Статус отправления",
					ct: "extensions",
				},
			],
			demo: null,
		},
		{
			id: 22,
			title: "Чат-бот, отвечающий на типовые запросы клиентов",
			slug: "cat-bot-otvecaiushhii-na-tipovye-zaprosy-klientov",
			icon: null,
			demo_image:
				"https://console.bot-marketing.com/storage/WUBvWVn2Pba0R3d26LUVOyeXLdm2FijO24IXS5gj.png",
			video_id: "https://youtu.be/hvpr_Savpx8",
			story_url: null,
			short_description:
				"Чат-бот, который будет круглосуточно отвечать на типовые вопросы клиентов, экономя время операторов.",
			short_promo_text:
				"<p>Чат-бот на основе NLP-модели, распознающий естественную речь. Понимает разные формулировки запроса, выбирает и присылает ответ на типовой вопрос. Чат-бот работает с уточнением адреса, вопросами по оформлению заказа, оплаты, условиям доставки, возврата и так далее.</p>",
			product: {
				id: 14,
				code: "umn-sol-otvetnazapros",
				price: 2500,
				originalPrice: {
					amount: 250000,
					currency: "RUB",
					formatted: "2 500 ₽",
				},
				currentPrice: {
					amount: 250000,
					currency: "RUB",
					formatted: "2 500 ₽",
				},
				discountPrice: {
					amount: 0,
					currency: "RUB",
					formatted: "0 ₽",
				},
				currency: "RUB",
				priceFormatted: "2 500,00 ₽",
				clientsLimit: 0,
			},
			tags: [
				{
					id: 13,
					lb: "Консультант",
					ct: "extensions",
				},
			],
			demo: null,
		},
		{
			id: 23,
			title: "Чат-бот, предлагающий клиенту подборку товаров в ответ на запрос",
			slug: "cat-bot-predlagaiushhii-klientu-podborku-tovarov-v-otvet-na-zapros",
			icon: null,
			demo_image: null,
			video_id: "https://youtu.be/hvpr_Savpx8",
			story_url: null,
			short_description:
				"Ориентируясь на запрос клиента, чат-бот прямо в чате предложит подборку наиболее подходящих товаров.",
			short_promo_text:
				"<p>Виртуальный продавец-консультант, понимает запрос клиента в свободной форме и предлагает наиболее подходящие позиции. Оформить заказ можно прямо в мессенджере.&nbsp;&nbsp;</p>",
			product: {
				id: 26,
				code: "Чат-бот-подборка-1",
				price: 2500,
				originalPrice: {
					amount: 250000,
					currency: "RUB",
					formatted: "2 500 ₽",
				},
				currentPrice: {
					amount: 250000,
					currency: "RUB",
					formatted: "2 500 ₽",
				},
				discountPrice: {
					amount: 0,
					currency: "RUB",
					formatted: "0 ₽",
				},
				currency: "RUB",
				priceFormatted: "2 500,00 ₽",
				clientsLimit: 0,
			},
			tags: [
				{
					id: 2,
					lb: "Магазин",
					ct: "business",
				},
				{
					id: 13,
					lb: "Консультант",
					ct: "extensions",
				},
			],
			demo: null,
		},
	],
};
