export default {
	id: 123,
	title: "Решение №1",
	slug: "reshenie-1",
	icon: "https://example.com/icon.png",
	demo_image: "https://example.com/demo.gif",
	product: {
		id: 123,
		code: "SOL-1",
		price: 2500,
		currency: "RUB",
		priceFormatted: "2500 RUB",
		clientsLimit: 2500,
		partnership: {
			links: [
				{
					title: "Реферальная ссылка",
					url: "https://example.com",
				},
				{
					title: "Реферальная ссылка N2",
					url: "https://app.swaggerhub.com/apis/lazychaser/tunnels_public_api/1.0.0#/Public%20API/get_public_catalog__id_",
				},
				{
					url: "https://app.swaggerhub.com/apis/lazychaser/tunnels_public_api",
				},
			],
			remuneration_tables: [
				{
					title: "Вознаграждения за установку решения",
					headers: ["Месяц абон платы", "Вознаграждение"],
					rows: [["Месяц 1", 500]],
				},
				{
					headers: ["Месяц абон платы", "Вознаграждение"],
					rows: [
						["Месяц 1", 500],
						["Месяц 1", 500],
						["Месяц 1", 500],
						["Месяц 1", 500],
						["Месяц 1", 500],
						["Месяц 1", 500],
						["Месяц 1", 500],
					],
				},
				{
					title:
						"Еще одна таблица с очень очень очень очень очень очень очень очень очень очень очень очень очень очень длинным названием",
					headers: ["Месяц абон платы", "Вознаграждение", "Nhtnmz rjkjyrf"],
					rows: [
						["Месяц 1", 500, "dasdas das dasd "],
						["Месяц 1", 500, "dasdas das dasd "],
						["Месяц 1", 500, "dasdas das dasd "],
					],
				},
			],
		},
	},
	short_description: "string",
	short_promo_text: "string",
	story_url: "string",
	demo: {
		telegram: true,
		whatsapp: true,
	},
	tags: [
		{
			id: 123,
			lb: "Avito",
			ct: "business",
			solutions: 0,
		},
	],
	description: "string",
	features: "string",
};
