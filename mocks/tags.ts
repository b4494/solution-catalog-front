export declare namespace Tags {
	interface Tag {
		id?: number;
		lb: string;
		ct: string;
		localizedCategory?: string;
		solutions?: number;
		_checked?: boolean;
		_search?: boolean;
	}
}

export default [
	{
		id: 1,
		lb: "avito",
		ct: "classifieds",
	},
	{
		id: 5,
		lb: "FAQ",
		ct: "business",
	},
	{
		id: 2,
		lb: "Магазин",
		ct: "business",
	},
	{
		id: 4,
		lb: "обращение",
		ct: "business",
	},
	{
		id: 6,
		lb: "ответ",
		ct: "business",
	},
	{
		id: 3,
		lb: "перевод",
		ct: "business",
	},
];
