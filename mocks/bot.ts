export interface BotInfo {
	id: number;
	title: string;
	slug: string;
	icon: string;
	demo_image?: any;
	short_description: string;
	short_promo_text?: string;
	product: Product;
	tags: Tag[];
	demo: any[];
	video_id?: any;
}

interface Tag {
	id: number;
	lb: string;
	ct: string;
}

interface Product {
	id: number;
	code: string;
	price: number;
	originalPrice: OriginalPrice;
	currentPrice: OriginalPrice;
	discountPrice: OriginalPrice;
	currency: string;
	priceFormatted: string;
	clientsLimit: number;
}

interface OriginalPrice {
	amount: number;
	currency: string;
	formatted: string;
}

export default {
	id: 3,
	title: "Решение №3: ITR меню",
	slug: "resenie-3-itr-meniu",
	icon: "https://console.bot-marketing.com/storage/k89CeNO6l4YmdIEdqiv15CqvzAAReus8W76NPzjO.svg",
	demo_image: null,
	short_description:
		"Позволяет настроить ITR меню по своему вкусу. Умеет переводить на оператора. Перезапуск по ключевым словам.",
	product: {
		id: 3,
		code: "umn-sol-3",
		price: 2500,
		originalPrice: {
			amount: 250000,
			currency: "RUB",
			formatted: "2 500 ₽",
		},
		currentPrice: {
			amount: 250000,
			currency: "RUB",
			formatted: "2 500 ₽",
		},
		discountPrice: {
			amount: 0,
			currency: "RUB",
			formatted: "0 ₽",
		},
		currency: "RUB",
		priceFormatted: "2 500,00 ₽",
		clientsLimit: 0,
	},
	tags: [
		{
			id: 1,
			lb: "avito",
			ct: "classifieds",
		},
	],
	demo: [],
};
