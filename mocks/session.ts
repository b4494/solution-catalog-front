export declare namespace Session {
	interface Response {
		user: User;
		app: App;
	}

	interface App {
		type: string;
		name: string;
		hash: string;
	}

	interface User {
		name: string;
		hash: string;
	}
}

export default {
	user: {
		name: "string",
		hash: "string",
	},
	app: {
		type: "string",
		name: "string",
		hash: "string",
	},
};
