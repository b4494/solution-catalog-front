export declare namespace Solution {
	interface Response {
		id: number;
		title: string;
		slug: string;
		icon: string;
		demo_image?: any;
		video_id?: any;
		story_url?: any;
		short_description: string;
		short_promo_text: string;
		product: Product;
		features?: any;
		description: string;
		tags: Tag[];
		demo: any[] | null | object;
	}

	interface Tag {
		id: number;
		lb: string;
		ct: string;
	}

	interface Product {
		id: number;
		code: string;
		price: number;
		originalPrice: OriginalPrice;
		currentPrice: OriginalPrice;
		discountPrice: OriginalPrice;
		currency: string;
		priceFormatted: string;
		clientsLimit: number;
		variants: any[];
		partnership?: any;
	}

	interface OriginalPrice {
		amount: number;
		currency: string;
		formatted: string;
	}
}

export default {
	id: 6,
	title: "Telegram WebApp-магазин",
	slug: "telegram-webapp-magazin",
	icon: "https://console.bot-marketing.com/storage/Nu0Le4xbm8VzUasFBoVKlXJR5QwbBueYqmLYm8ey.svg",
	demo_image: null,
	video_id: null,
	story_url: null,
	short_description:
		"Магазин с товарами, который будет открываться прямо в Telegram.",
	short_promo_text:
		"Чат-бот в телеграм, в котором можно открыть каталог с товарами и оформить заказ. Вам нужно только заполнить товары в Umnico и Ваш магазин в telegram готов.",
	product: {
		id: 10,
		code: "umn-sol-telegramshop",
		price: 2500,
		originalPrice: { amount: 250000, currency: "RUB", formatted: "2 500 ₽" },
		currentPrice: { amount: 250000, currency: "RUB", formatted: "2 500 ₽" },
		discountPrice: { amount: 0, currency: "RUB", formatted: "0 ₽" },
		currency: "RUB",
		priceFormatted: "2 500,00 ₽",
		clientsLimit: 0,
		variants: [],
	},
	features: null,
	description:
		"С недавнего времени в Telegram появилась возможность открывать web-app/сайты прямо из интерфейса чат-бота. А значит, теперь в Telegram можно сделать полноценный интернет-магазин. Путь клиента становится короче: оформить заказ и оплатить его можно не выходя из мессенджера. Более того, каждый покупатель (и даже потенциальный покупатель), запустивший чат-бот становится вашим подписчиком. Ему можно отправлять рассылки, рассказывать про эксклюзивные предложения и акции.",
	tags: [{ id: 2, lb: "Магазин", ct: "business" }],
	demo: [],
};
