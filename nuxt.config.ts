import bmConfig from "./bm-catalog.config";
import { createMemoryHistory } from "vue-router";
import { ru } from "./i18n/ru";
import { en } from "./i18n/en";

export default defineNuxtConfig({
	css: [
		"~/assets/scss/main.scss",
		"bootstrap/dist/css/bootstrap.min.css",
		"~/assets/scss/bt-remapping.css",
	],
	app: {
		baseURL: bmConfig.BASE_URL ?? "",
	},

	modules: ["@pinia/nuxt", "@nuxtjs/i18n", "@vueuse/nuxt"],

	//@ts-ignore
	i18n: {
		vueI18n: {
			legacy: false,
			locale: "ru",
			messages: { ru, en },
		},
	},

	plugins: ["~/plugins/floating-vue"],
	ssr: false,

	runtimeConfig: {
		app: {
			baseURL: bmConfig.BASE_URL ?? "",
		},
		public: {
			defaultIntegration: process.env.APP_DEFAULT_INTEGRATION,
			defaultBaseDomain: process.env.APP_DEFAULT_BASE_DOMAIN,
		},
	},

	vite: {
		css: {
			preprocessorOptions: {
				scss: {
					additionalData: [
						`@import "./assets/scss/functions";
						 @import "./assets/scss/variables";`,
					],
				},
			},
		},
	},
});
