export default defineEventHandler((event) => {
	return {
		integration: event.node.req.headers["x-integration"],
		baseDomain: event.node.req.headers["x-base-domain"],
		basePath: event.node.req.headers["x-base-path"],
	};
});
