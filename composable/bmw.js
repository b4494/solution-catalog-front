import config from "~/bm-catalog.config";

export function useWidget() {
	(function (w, d, e, s, i) {
		w[i] =
			w[i] ||
			function () {
				(w[i].a = w[i].a || []).push(arguments);
			};

		let k = d.createElement(e);
		let a = d.getElementsByTagName(e)[0];
		k.async = 1;
		k.src = s;
		a.parentNode.insertBefore(k, a);
	})(window, document, "script", "https://mssg.su/widget/main.js", "bmw");

	bmw(
		config.WIDGET_CODE,
		"init",
		"https://marketing.chat2desk.com/api/public/widgets"
	);
}
