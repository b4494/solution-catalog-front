import { useResizeObserver } from "@vueuse/core";
import { computed, nextTick, onMounted, ref, watchEffect } from "vue";
/**
 * @typedef { import("vue").Ref<Number> } refNumber
 * @typedef { import("vue").Ref<Array> } refArray
 */
/**
 * @typedef {Object} CropList
 * @property {Boolean} isFull List is full or not
 * @property {Boolean} canBeCropped
 * @property {refArray} list Ref array
 * @property {refArray} croppedList Ref cropped array part
 * @property {refNumber} currentLength Ref current length
 * @property {refNumber} croppedLength Ref cropped array length
 * @property {Function} open Show all items
 * @property {Function} close Hide items
 * @property {Function} toggle Toggle show/hide
 */

/**
 * Returns reactive Cropped list fields
 * @param {Array | refArray} listToCrop List reference
 * @param {(Number | refNumber)} desiredLength
 * @returns {CropList} Cropped list fields
 */
export const useCropList = function (
	listToCrop,
	desiredLength,
	isOpen = false
) {
	listToCrop = ref(listToCrop);
	desiredLength = ref(desiredLength);
	isOpen = ref(isOpen);

	const canBeCropped = computed(
		() => desiredLength.value < listToCrop.value.length
	);
	const list = ref([]);
	const croppedList = ref([]);
	const currentLength = computed(() => list.value.length);
	const croppedLength = computed(() => croppedList.value.length);
	watchEffect(() => {
		if (canBeCropped.value && !isOpen.value) {
			list.value = listToCrop.value.slice(0, desiredLength.value);
			croppedList.value = listToCrop.value.slice(desiredLength.value);
		} else {
			list.value = listToCrop.value;
			croppedList.value = [];
		}
	});
	const functions = {
		open: () => (isOpen.value = true),
		close: () => (isOpen.value = false),
		toggle: () => (isOpen.value = !isOpen.value),
	};

	return {
		isOpen,
		canBeCropped,
		list,
		croppedList,
		currentLength,
		croppedLength,
		...functions,
	};
};

/**
 * Fill container with flex nowrap items from array.
 * @param {refArray} listWrapEl DOM reference
 * @param {refArray} listToCrop List reference
 * @param {Number | refNumber} desiredLength Initial desired length
 * @returns {CropList} Cropped list fields
 */
export const useCropListFill = function (
	listWrapEl,
	listToCrop,
	desiredLength = 15,
	padding = 0
) {
	const dLength = ref(desiredLength);
	onMounted(update);
	onUpdated(update);

	useResizeObserver(listWrapEl, () => {
		update();
	});

	async function update() {
		if (listWrapEl.value) {
			while (isOverflowing(listWrapEl.value) && dLength.value > 1) {
				dLength.value--;
				await nextTick();
			}
		}
	}

	function isOverflowing(listWrapElement) {
		let firstChild = listWrapElement.firstChild;
		while (firstChild != null && firstChild.nodeType == 3) {
			firstChild = firstChild.nextSibling;
		}

		return (
			listWrapElement.offsetWidth - padding <
			listWrapElement.firstChild.scrollWidth
		);
	}

	return useCropList(listToCrop, dLength);
};
