import { useTimeId } from "./unique-id";

type ModalType = "install" | "demo" | "contact";

interface ModalSetup {
	openModal: Function;
	modalID: string;
	modalType: Ref<ModalType>;
}

export function useSetupModal(): ModalSetup {
	const modalType = ref("install" as ModalType);
	const modalID = "m-" + useTimeId();

	function openModal(type: "install" | "demo" | "contact") {
		modalType.value = type;
	}

	return {
		openModal,
		modalID,
		modalType,
	};
}
