import { Endpoint } from "~/stores/endpoints";
import { useIntegrationStore } from "~~/stores/integration";

// @ts-ignore
import QueryString from "qs";

const API_URL = "api/public";

type FetchMethod = "get" | "post" | "put" | "patch" | "delete";

export function useCustomFetch(
	endpoint: Endpoint,
	params?: { [key: string]: any }
) {
	let data = ref({} as any);
	const $integrationStore = useIntegrationStore();

	// prettier-ignore
	const url = `https://${$integrationStore.BASE_DOMAIN}/${API_URL}/${endpoint.url}`;

	async function fetchFunction(url: string, method: FetchMethod) {
		if (method === "get") {
			//. Add 'lang'-query to all GET-params
			params = Object.assign(params ?? {}, {
				lang: $integrationStore.LANG,
			});

			url += paramsSerializer(params);

			return useFetch(url)
				.then((r) => (data.value = r?.data?.value))
				.catch((e) => console.log(e));
		}

		const options = { method };

		return useFetch(url, options).catch((e) => console.log(e));
	}

	// prettier-ignore
	return {
		get: 		() => fetchFunction(url, "get"),
		post: 	() => fetchFunction(url, "post"),
		put: 		() => fetchFunction(url, "put"),
		remove: () => fetchFunction(url, "delete"),
		patch: () => fetchFunction(url, "patch"),
		data
	};
}

function paramsSerializer(params: any): string {
	Object.keys(params).forEach((key) => {
		if (params[key] === "") params[key] = undefined;
		if (params[key] === null) params[key] = undefined;
	});

	return (
		"?" +
		QueryString.stringify(params, {
			arrayFormat: "indices",
			strictNullHandling: true,
		})
	);
}
