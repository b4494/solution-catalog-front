import moment from "moment";

export function useTimeId(negative = false): number {
	const id = +moment().format("x");
	return negative ? -id : id;
}
