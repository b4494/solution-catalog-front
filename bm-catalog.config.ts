interface BmConfig {
	WHATSAPPP_PHONE?: string;
	TELEGRAM_BOT?: string;
	WIDGET_CODE: string;
	DISABLE_CUSTOM_STYLES?: boolean;
	BASE_URL?: string;
	DEFAULT_TEXT_FILTERS?: string[];
	DEFAULT_LANG: "ru" | "en";
}

export default {
	WHATSAPPP_PHONE: "79993334235",
	TELEGRAM_BOT: "marketing_support_bot",
	WIDGET_CODE: "zKV3C912",
	DISABLE_CUSTOM_STYLES: false,
	BASE_URL: "/catalog/",
	DEFAULT_TEXT_FILTERS: ["Магазин", "Ответ", "Перевод", "FAQ", "Чат-бот"],
	DEFAULT_LANG: "ru",
} as BmConfig;
