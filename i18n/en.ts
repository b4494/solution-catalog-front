export const en = {
	buttons: {
		create_bot: "Create bot",
		order_solution: "Order a solution",
		watch_demo: "Watch demo",
		filters: "Filters",
		more: "More",
		expand: "Expand",
		hide: "Hide",
		all_solutions: "All solutions",
		copy: "Copy",
		copy_done: "Сopied",
		become_partner: "Become a partner",
		install_solution: "Install",
	},

	modal: {
		title: {
			contact: "Choose the messenger to contact support",
			install: "Choose the messenger to install",
			demo: "Choose the messenger to watch demo",
		},
		message: {
			contact: "I'd like to order the bot development",
			install: "I'd like to order ",
			demo: "I'd like to watch demo of ",
		},
		subtitle: "Choose the messenger that is installed on your device",
	},

	catalog: {
		all_bots: "All bots",
		search_placeholder: "Search by name",
		title: "Use the search or create a bot",
		noresults: "No results found",
		noresults_sub: "Try to refine your search or use one of the filters below",
		default_filters: ["Market", "Answer", "Translate", "FAQ", "Chat-bot"],
	},

	solution: {
		price: "Price",
		from: "from",
		onrequest: "On request",
		description: "Description",
		features: "Features",
		partnership: "Partnership",
	},
};
