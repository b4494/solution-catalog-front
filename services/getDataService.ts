import type { BotInfo } from "~~/mocks/bot";
import type { Tags } from "~~/mocks/tags";
import type { Solution } from "~~/mocks/solution";
import type { Session } from "~~/mocks/session";
import type { Catalog } from "~~/mocks/catalog";

import { useCustomFetch } from "~~/composable/fetch";
import { endpoints } from "~~/stores/endpoints";

export async function getBotList(params: any): Promise<Catalog.Response> {
	const { get, data } = useCustomFetch(endpoints().catalog.list, params);
	await get();
	return data.value;
}

export async function getTags(): Promise<Tags.Tag[]> {
	const { get, data } = useCustomFetch(endpoints().catalog.tags);
	await get();
	return data.value ?? ([] as Tags.Tag[]);
}

export async function getSolutionByID(id: number): Promise<Solution.Response> {
	const { get, data } = useCustomFetch(endpoints().solution(id));
	await get();
	return data.value;
}

export async function getSession(): Promise<Session.Response> {
	const { get, data } = useCustomFetch(endpoints().session);
	await get();
	return data.value;
}
