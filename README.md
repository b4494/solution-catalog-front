# bm-catalog

This template should help get you started developing with Nuxt3.

## Nuxt 3 Minimal Starter

Look at the [Nuxt 3 documentation](https://nuxt.com/docs/getting-started/introduction) to learn more.

## Recommended IDE Setup

[VSCode](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=johnsoncodehk.volar) (and disable Vetur) + [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=johnsoncodehk.vscode-typescript-vue-plugin).

## Installation

Make sure to install the dependencies:

```bash
# yarn
yarn install

# npm
npm install

# pnpm
pnpm install --shamefully-hoist
```

## Development Server

Start the development server on http://localhost:3000

```bash
npm run dev
```

Also you have to add ".env" file in the root directory with following variables.

It is necessary for mocking external variables that are not available in dev-mode.

```
APP_DEFAULT_INTEGRATION: string
APP_DEFAULT_BASE_DOMAIN: string
```

## Production

Build the application in development-mode:

```bash
npm run build
```

Build the application for production:

```bash
npm run prod
```

## Bm-catalog configuration

Use the root file [bm-catalog.config.ts](bm-catalog.config.ts) to set the following properties:

```bash
WHATSAPPP_PHONE?:       string
TELEGRAM_BOT?:          string
WIDGET_CODE:            string
BASE_URL?:              string          // begin width "/"
DISABLE_CUSTOM_STYLES?: boolean         // disable load slyles file 'https://${baseDomain}/css/${integration}/app.css'
DEFAULT_TEXT_FILTERS:   string[]        // array of query-filters for the "no-results" screen
DEFAULT_LANG:           "ru" | "en",    // default value for localization
```

## Additional CSS-variables

Use the following css-variables to make additional customization.

You can see default values here: [\_variables.scss](assets/scss/_variables.scss)

`colors:`

```
--bc--color--border-divider:      divider color

--bc--color--bg-primary:          main background color
--bc--color--bg-secondary:        secondary background color
--bc--color--bg-aside:            aside-bar background color
--bc--color--bg-card:             cards/tiles background color
--bc--color--bg-filterchips:      filterchips background color
--bc--color--bg-badge-secondary:  secondary background color of badge
--bc--color--bg-badge-dark:       dark background color of badge
--bc--color--bg-badge-darkest:    darkest background color of badge
```

`sizes`

```
--bc--aside-width:                aside-bar width
```

Check out the [deployment documentation](https://nuxt.com/docs/getting-started/deployment) for more information.

## Nginx Reverse Proxy

If the app is running under docker, nginx reverse proxy config should be similar to (X-INTEGRATION header will be passed to app)

```bash
map $sent_http_content_type $expires {
    "text/html"                 epoch;
    "text/html; charset=utf-8"  epoch;
    default                     off;
}

server {
    listen       8072;

    resolver 127.0.0.11 ipv6=off;

    server_name  bm-catalog.demo.com;

    access_log /var/log/nginx/bm-catalog.demo.com-access.log;
    error_log /var/log/nginx/bm-catalog.demo.com-error.log;

    root /var/www/;

    gzip            on;
    gzip_types      text/plain application/xml text/css application/javascript;
    gzip_min_length 1000;

    location / {

        expires $expires;
        proxy_redirect off;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-LANG en;
        proxy_set_header X-INTEGRATION umnico;
	proxy_set_header X-BASE-DOMAIN console.bot-marketing.com;
        proxy_read_timeout 1m;
        proxy_connect_timeout 1m;
        proxy_pass http://node:3000;

    }
}
```
